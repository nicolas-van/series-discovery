# Series Discovery

A wonderful application to discover new series.

See deployment here: https://nicolas-van.gitlab.io/series-discovery

## Quickstart guide

```
npm install
npm run start
```

export const environment = {
  production: true,
  tmdbApiKey: 'a8b770ec75a94b1fc5412c5de6fa33e3',
  tmdbRoot: 'https://api.themoviedb.org/3',
  tmdbImagesRoot: 'https://image.tmdb.org/t/p',
};

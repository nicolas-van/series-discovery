import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import Serie from './Serie';

@Injectable({
  providedIn: 'root'
})
export class SeriesDatabaseService {

  constructor(
    private http: HttpClient,
  ) {
  }

  /*
    Gets a list of popular series.

    page: the asked page
  */
  getPopular(page: number = 1): Observable<Serie[]> {
    return this.http.get(
      `${env.tmdbRoot}/tv/popular?api_key=${env.tmdbApiKey}&language=en&page=${page}`).
      pipe(map((res: any) => res.results));
  }
  /*
    Gets a specific serie by id.

    id: the id of the movie
  */
  getSerie(id: number): Observable<Serie> {
    return this.http.get<Serie>(
      `${env.tmdbRoot}/tv/${id}?api_key=${env.tmdbApiKey}`);
  }

  /*
    Search for series.

    query: the search query
    page: the asked page
  */
  search(query: string, page: number = 1): Observable<Serie[]> {
    return this.http.get(
      `${env.tmdbRoot}/search/tv?language=en&query=${encodeURI(query)}&api_key=${env.tmdbApiKey}&page=${page}`).
      pipe(map((res: any) => res.results));
  }
}

import { Component, OnInit, Input } from '@angular/core';
import Serie from '../Serie';

/*
  Displays a miniature of a serie.
*/
@Component({
  selector: 'app-serie-miniature',
  templateUrl: './serie-miniature.component.html',
  styleUrls: ['./serie-miniature.component.scss']
})
export class SerieMiniatureComponent implements OnInit {
  
  @Input() serie: Serie;

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerieMiniatureComponent } from './serie-miniature.component';

describe('SerieMiniatureComponent', () => {
  let component: SerieMiniatureComponent;
  let fixture: ComponentFixture<SerieMiniatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerieMiniatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerieMiniatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


export default interface Serie {
  name: string;
  first_air_date: string;
  poster_path: string;
  posterUrl: string;
  id: number;
  backdropUrl: string;
  backdrop_path: string;
  overview: string;
}

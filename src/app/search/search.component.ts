import { Component, OnInit } from '@angular/core';
import Serie from '../Serie';
import { Subject } from 'rxjs';
import * as rxop from 'rxjs/operators';
import { environment as env } from '../../environments/environment';
import { SeriesDatabaseService } from '../series-database.service';

/*
  A page to search for series.
*/
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  
  series: Serie[];
  currentPage: number;
  search: string;
  searchSubject: Subject<string>;

  constructor(
    private seriesDatabase: SeriesDatabaseService,
  ) { }

  ngOnInit() {
    this.series = [];
    this.currentPage = 0;
    this.search = "";
    this.searchSubject = new Subject<string>();
    
    this.searchSubject.pipe(rxop.auditTime(1000)).subscribe((next: string) => {
      this.search = next;
      this.searchNext(true);
    });
  }
  
  changeSearch(next: string) {
    this.searchSubject.next(next);
  }
  
  searchNext(restart: boolean = false) {
    if (restart) {
      this.currentPage = 0;
      this.series = [];
    }
    if (this.search === "") {
      this.series = [];
    } else {
      this.currentPage += 1;
      this.seriesDatabase.search(this.search, this.currentPage).subscribe((series: Serie[]) => {
        for (const serie of series) {
          serie.posterUrl = serie.poster_path ? `${env.tmdbImagesRoot}/original${serie.poster_path}` : null;
        }
        this.series = this.series.concat(series);
      });
    }
  }
  
  onScroll() {
    this.searchNext(false);
  }

}

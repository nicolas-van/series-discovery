import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Serie from '../Serie';
import { SeriesDatabaseService } from '../series-database.service';
import { environment as env } from '../../environments/environment';

/*
  A page to display a serie.
*/
@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.scss']
})
export class SerieComponent implements OnInit {
  
  serie: Serie;

  constructor(
    private route: ActivatedRoute,
    private seriesDatabase: SeriesDatabaseService,
  ) { }

  ngOnInit() {
    const id: number = +this.route.snapshot.paramMap.get('id');
    this.serie = null;
    
    this.seriesDatabase.getSerie(id).subscribe((serie: Serie) => {
      serie.posterUrl = serie.poster_path ? `${env.tmdbImagesRoot}/original${serie.poster_path}` : null;
      serie.backdropUrl = serie.backdrop_path ? `${env.tmdbImagesRoot}/original${serie.backdrop_path}` : null;
      this.serie = serie;
    });
    
  }

}

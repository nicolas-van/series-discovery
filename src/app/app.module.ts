import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PopularSeriesComponent } from './popular-series/popular-series.component';
import { RouterModule, Routes } from '@angular/router';
import { SerieMiniatureComponent } from './serie-miniature/serie-miniature.component';
import { SerieComponent } from './serie/serie.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  { path: '', component: PopularSeriesComponent },
  { path: 'search', component: SearchComponent },
  { path: 'serie/:id', component: SerieComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    PopularSeriesComponent,
    SerieMiniatureComponent,
    SerieComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true }),
    InfiniteScrollModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

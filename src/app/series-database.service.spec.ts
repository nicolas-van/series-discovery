import { TestBed, inject } from '@angular/core/testing';

import { SeriesDatabaseService } from './series-database.service';

describe('SeriesDatabaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeriesDatabaseService]
    });
  });

  it('should be created', inject([SeriesDatabaseService], (service: SeriesDatabaseService) => {
    expect(service).toBeTruthy();
  }));
});

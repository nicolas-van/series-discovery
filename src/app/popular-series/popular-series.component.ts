import { Component, OnInit } from '@angular/core';
import { SeriesDatabaseService } from '../series-database.service';
import { environment as env } from '../../environments/environment';
import Serie from '../Serie';

/*
  Displays a list of popular series.
*/
@Component({
  selector: 'app-popular-series',
  templateUrl: './popular-series.component.html',
  styleUrls: ['./popular-series.component.scss']
})
export class PopularSeriesComponent implements OnInit {

  series: Serie[];
  currentPage: number;

  constructor(
    private seriesDatabase: SeriesDatabaseService,
  ) { }

  ngOnInit() {
    this.series = [];
    this.currentPage = 0;
    this.queryNext();
  }
  
  queryNext() {
    this.currentPage += 1;
    this.seriesDatabase.getPopular(this.currentPage).subscribe((series: Serie[]) => {
      for (const serie of series) {
        serie.posterUrl = serie.poster_path ? `${env.tmdbImagesRoot}/original${serie.poster_path}` : null;
      }
      this.series = this.series.concat(series);
    });
  }
  
  onScroll() {
    this.queryNext();
  }

}
